Инструкция деплоя приложения в Tomcat
=============================

REQUIREMENTS
------------

Должны быть предустановлены и запущены:

       Apache Tomcat версии 8.5.23 или выше
	   База данных Oracle Database 12c
	   
INSTALLATION
------------

 - Переименовать WAR архив приложения **gs-producting-web-service-1.0.0.war** в **bank.war**
 - Скопировать WAR архив **bank.war** в директорию томката **webapps**: _..\apache-tomcat\webapps_
 
SETTINGS
-----------
После деплоя приложения в Tomcat, в директории _..\apache-tomcat\webapps\bank\WEB-INF\classes_ появится файл **application.properties** который можно открыть текстовым редактором.
В файле **application.properties** имеются 3 параметра (использовать 1 из 3, остальные отключить символом **#**):

       spring.jpa.hibernate.ddl-auto=create - создаёт необходимые таблицы в бд, если такие отсутствуют.
       spring.jpa.hibernate.ddl-auto=create-drop - создаёт необходимые таблицы в бд, если такие отсутствуют. А при остановке приложения удаляет используемые таблицы.
       spring.jpa.hibernate.ddl-auto=validate - не вносит изменение в бд при запуске\остановке приложения, используется для валидации соществующих таблиц бд при запуске приложения.
	   
Так же в файле присутствуют настройки логирования, подключения к БД и прочее, например:

       spring.datasource.username=oracle - логин БД
       spring.datasource.password=oracle - пароль БД
	   
**Все изменения в файле _application.properties_ применяются после перезапуска приложения**
 
START
-----------
После установки приложение доступно по адресу [http://localhost:8080/bank/](http://localhost:8080/bank/)
