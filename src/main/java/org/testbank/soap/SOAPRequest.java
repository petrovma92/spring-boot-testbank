package org.testbank.soap;

import javax.xml.soap.*;

public class SOAPRequest {
    private SOAPMessage message;

    public SOAPRequest(String... parameters) throws Exception {
        this.message = createSOAPRequest(parameters);
    }

    private SOAPMessage createSOAPRequest(String... parameters) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, parameters);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "");

        soapMessage.saveChanges();

        return soapMessage;
    }

    private void createSoapEnvelope(SOAPMessage soapMessage, String... parameters) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();
        for(int i = 0; i < parameters.length; i++)
            if(parameters[i]==null)
                parameters[i] = "";

        String namespace = "gs";
        String namespaceURI = "http://spring.io/guides/gs-producing-web-service";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(namespace, namespaceURI);

        SOAPElement soapBodyElem1, soapBodyElem;
        SOAPBody soapBody = envelope.getBody();

        if(parameters.length > 4) {
            soapBodyElem = soapBody.addChildElement("sendDataRequest", namespace);
            soapBodyElem1 = soapBodyElem.addChildElement("firstName", namespace);
            soapBodyElem1.addTextNode(parameters[0]);
            soapBodyElem1 = soapBodyElem.addChildElement("secondName", namespace);
            soapBodyElem1.addTextNode(parameters[1]);
            soapBodyElem1 = soapBodyElem.addChildElement("passportSeries", namespace);
            soapBodyElem1.addTextNode(parameters[2]);
            soapBodyElem1 = soapBodyElem.addChildElement("passportNumber", namespace);
            soapBodyElem1.addTextNode(parameters[3]);
            soapBodyElem1 = soapBodyElem.addChildElement("phone", namespace);
            soapBodyElem1.addTextNode(parameters[4]);
            soapBodyElem1 = soapBodyElem.addChildElement("birthDate", namespace);
            soapBodyElem1.addTextNode(parameters[5]);
            soapBodyElem1 = soapBodyElem.addChildElement("work", namespace);
            soapBodyElem1.addTextNode(parameters[6]);
            soapBodyElem1 = soapBodyElem.addChildElement("salary", namespace);
            soapBodyElem1.addTextNode(parameters[7]);
            soapBodyElem1 = soapBodyElem.addChildElement("badHabits", namespace);
            soapBodyElem1.addTextNode(parameters[8]);
            soapBodyElem1 = soapBodyElem.addChildElement("clientCar", namespace);
            soapBodyElem1.addTextNode(parameters[9]);
            soapBodyElem1 = soapBodyElem.addChildElement("richRelative", namespace);
            soapBodyElem1.addTextNode(parameters[10]);
            soapBodyElem1 = soapBodyElem.addChildElement("aircraft", namespace);
            soapBodyElem1.addTextNode(parameters[11]);
            soapBodyElem1 = soapBodyElem.addChildElement("contactPerson", namespace);
            soapBodyElem1.addTextNode(parameters[12]);
        }
        else {
            soapBodyElem = soapBody.addChildElement("getAppRequest", namespace);
            soapBodyElem1 = soapBodyElem.addChildElement("firstName", namespace);
            soapBodyElem1.addTextNode(parameters[0]);
            soapBodyElem1 = soapBodyElem.addChildElement("secondName", namespace);
            soapBodyElem1.addTextNode(parameters[1]);
            soapBodyElem1 = soapBodyElem.addChildElement("passportSeries", namespace);
            soapBodyElem1.addTextNode(parameters[2]);
            soapBodyElem1 = soapBodyElem.addChildElement("passportNumber", namespace);
            soapBodyElem1.addTextNode(parameters[3]);
        }
    }

    public SOAPMessage getMessage() {
        return message;
    }
}
