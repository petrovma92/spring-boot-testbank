package org.testbank;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/ws/*");
	}

	@Bean(name = "sentSoapData")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema soapBankSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("BankPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setTargetNamespace("http://spring.io/guides/gs-producing-web-service");
		wsdl11Definition.setSchema(soapBankSchema);
		return wsdl11Definition;
	}
//	@Bean
//	public XsdSchemaCollection quotesSchemaCollection() {
//		return new XsdSchemaCollection() {
//
//			public XsdSchema[] getXsdSchemas() {
//				return new CommonsXsdSchemaCollection(new ClassPathResource("soapBank.xsd"), new ClassPathResource("sentSoapData.xsd"));
//				return new XsdSchema[]{new SimpleXsdSchema(new ClassPathResource("soapBank.xsd")), new SimpleXsdSchema(new ClassPathResource("sentSoapData.xsd"))};
//			}
//
//			public XmlValidator createValidator() {
//				throw new UnsupportedOperationException();
//			}
//		};
//	}

	@Bean
	public XsdSchema soapBankSchema() {
		return new SimpleXsdSchema(new ClassPathResource("sentSoapData.xsd"));
	}

}
