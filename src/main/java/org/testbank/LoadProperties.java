package org.testbank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {
    private static final Logger LOG = LoggerFactory.getLogger(LoadProperties.class);
    private static String wayToSoapRequest = "http://localhost:8080/bank";

/*
    static {
        Properties prop = new Properties();
        InputStream input = null;
        try {

            input = new FileInputStream("src\\main\\resources\\soap.properties");

            // load a properties file
            prop.load(input);

            // get the property value
            wayToSoapRequest = prop.getProperty("url.to.soap.request");
            LOG.info("PROPERTY url.to.soap.request = " + wayToSoapRequest);

        } catch (IOException ex) {
            LOG.error("ERROR Load Property!!!", ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    LOG.error("ERROR Load Property!!!", e);
                }
            }
        }

    }
*/

    public static String getWayToSoapRequest() {
        return wayToSoapRequest != null ? wayToSoapRequest : "http://localhost:8080";
    }
}
