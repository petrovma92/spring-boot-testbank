package org.testbank.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.testbank.model.CreditApplication;
import org.testbank.repository.CreditApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.Collection;

@Service
public class CreditApplicationServiceImpl implements CreditApplicationService {

    private CreditApplicationRepository repository;

    @Autowired
    public CreditApplicationServiceImpl(CreditApplicationRepository repository) {
        this.repository = repository;
    }

    @Cacheable("applications")
    @Override
    public CreditApplication get(long id) {
        return repository.get(id);
    }

//    @Override
//    public void delete(int id) {
//        ExceptionUtil.checkNotFoundWithId(repository.delete(id), id);
//        repository.delete(id);
//    }

    @CacheEvict(value = "applications", allEntries = true)
    @Override
    public CreditApplication update(CreditApplication creditApplication) {
        Assert.notNull(creditApplication, "creditApplication must not be null");
        return repository.save(creditApplication);
    }

    @CacheEvict(value = "applications", allEntries = true)
    @Override
    public CreditApplication save(CreditApplication creditApplication) {
        Assert.notNull(creditApplication, "creditApplication must not be null");
        return repository.save(creditApplication);
    }

    @Cacheable("applications")
    @Override
    public Collection<CreditApplication> getBetweenDateTimes(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        Assert.notNull(startDateTime, "startDateTime must not be null");
        Assert.notNull(endDateTime, "endDateTime  must not be null");
        return repository.getBetween(startDateTime, endDateTime);
    }
}
