package org.testbank.service;


import org.testbank.model.User;

public interface UserService {

    User save(User user);

    User get(long id);

//    User getByPassport(long passport);

//    List<User> getAll();

    void update(User user);

    void evictCache();

//    User getWithApp(int id);
}
