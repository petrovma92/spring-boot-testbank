package org.testbank.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.testbank.model.Settings;
import org.testbank.model.User;
import org.testbank.repository.SettingsRepository;
import org.testbank.repository.UserRepository;

@Service
public class SettingsServiceImpl implements SettingsService {

    private SettingsRepository repository;

    @Autowired
    public SettingsServiceImpl(SettingsRepository repository) {
        this.repository = repository;
    }

    @CacheEvict(value = "settings", allEntries = true)
    @Override
    public Settings save(Settings settings) {
//        Assert.notNull(user, "user must not be null");
        return repository.save(settings);
    }

    @Cacheable("settings")
    @Override
    public Settings get(long id) {
        return repository.get(id);
    }

    @Cacheable("settings")
    @Override
    public Settings getByKey(String key) {
        return repository.getByKey(key);
    }

    @CacheEvict(value = "settings", allEntries = true)
    @Override
    public void evictCache() {
    }
}
