package org.testbank.service;


import org.testbank.model.Settings;

public interface SettingsService {

    Settings save(Settings user);

    Settings get(long id);

    Settings getByKey(String key);

    void evictCache();
}
