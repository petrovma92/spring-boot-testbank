package org.testbank.service;


import org.testbank.model.CreditApplication;

import java.time.LocalDateTime;
import java.util.Collection;

public interface CreditApplicationService {
    CreditApplication get(long id);

//    void delete(int id) throws NotFoundException;

    CreditApplication update(CreditApplication creditApplication);

    CreditApplication save(CreditApplication creditApplication);

    Collection<CreditApplication> getBetweenDateTimes(LocalDateTime startDateTime, LocalDateTime endDateTime);
}
