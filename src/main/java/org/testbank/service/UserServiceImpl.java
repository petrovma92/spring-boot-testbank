package org.testbank.service;

import org.testbank.model.User;
import org.testbank.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @CacheEvict(value = "users", allEntries = true)
    @Override
    public User save(User user) {
//        Assert.notNull(user, "user must not be null");
        return repository.save(user);
    }

//    @CacheEvict(value = "users", allEntries = true)
//    @Override
//    public void delete(int id) {
//        ExceptionUtil.checkNotFoundWithId(repository.delete(id), id);
//    }

    @Cacheable("users")
    @Override
    public User get(long id) {
        return repository.get(id);
    }

//    @Cacheable("users")
//    @Override
//    public User getByPassport(long passport) {
//        return repository.getByPassport(passport);
//    }

//    @Override
//    public User getByEmail(String email) throws NotFoundException {
//        Assert.notNull(email, "email must not be null");
//        return ExceptionUtil.checkNotFound(repository.getByEmail(email), "email=" + email);
//    }

//    @Cacheable("users")
//    @Override
//    public List<User> getAll() {
//        return repository.getAll();
//    }

    @CacheEvict(value = "users", allEntries = true)
    @Override
    public void update(User user) {
        Assert.notNull(user, "user must not be null");
        repository.save(user);
    }

    @CacheEvict(value = "users", allEntries = true)
    @Override
    public void evictCache() {
    }

//    @Override
//    public User getWithApp(int id) {
//        return ExceptionUtil.checkNotFoundWithId(repository.getWithMeals(id), id);
//    }
}
