package org.testbank.repository.datajpa;

import org.testbank.model.User;
import org.testbank.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class DataJpaUserRepositoryImpl implements UserRepository {
    private static final Logger LOG = LoggerFactory.getLogger(DataJpaUserRepositoryImpl.class);

    @Autowired
    private CrudUserRepository crudRepository;

    @Override
    public User save(User user) {
        return crudRepository.save(user);
    }

//    @Override
//    public boolean delete(int id) {
//        return crudRepository.delete(id) != 0;
//    }

    @Override
    public User get(long id) {
        return crudRepository.findOne(id);
    }

//    @Override
//    public User getByPassport(long passport) {
//        return crudRepository.getByPassport(passport);
//    }

//    @Override
//    public User getByEmail(String email) {
//        return crudRepository.getByEmail(email);
//    }

//    @Override
//    public List<User> getAll() {
//        return crudRepository.findAll();
//    }

//    @Override
//    public User getWithMeals(int id) {
//        return crudRepository.getWithMeals(id);
//    }
}
