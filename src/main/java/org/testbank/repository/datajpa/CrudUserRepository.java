package org.testbank.repository.datajpa;

import org.testbank.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface CrudUserRepository extends JpaRepository<User, Long> {

//    @Transactional
//    @Modifying
//    @Query("DELETE FROM User u WHERE u.id=:id")
//    int delete(@Param("id") int id);

    @Override
    @Transactional
    User save(User user);

    @Override
    @Query("SELECT u FROM User u WHERE u.id = ?1")
    User findOne(Long id);

//    @Query("SELECT u FROM User u WHERE u.passport = ?1")
//    User getByPassport(Long passport);

//    @Override
//    @Query("SELECT u FROM User u LEFT JOIN FETCH u.roles ORDER BY u.name, u.email")
//    List<User> findAll();

//    User getByEmail(String email);

//    @Query("SELECT u FROM User u LEFT JOIN FETCH u.meals WHERE u.id = ?1")
//    User getWithMeals(int id);
}
