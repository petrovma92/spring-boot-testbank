package org.testbank.repository.datajpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.testbank.model.CreditApplication;

import java.time.LocalDateTime;
import java.util.List;

@Transactional(readOnly = true)
public interface CrudApplicationRepository extends JpaRepository<CreditApplication, Long> {

//    @Modifying
//    @Transactional
//    @Query("DELETE FROM CreditApplication m WHERE m.id=:id AND m.user.id=:userId")
//    int delete(@Param("id") int id, @Param("userId") int userId);

    @Override
    @Transactional
    CreditApplication save(CreditApplication item);

    @Override
    CreditApplication findOne(Long aLong);

    //    @Query("SELECT m FROM CreditApplication m WHERE m.user.id=:userId ORDER BY m.dateTime DESC")
//    List<CreditApplication> getAll(@Param("userId") int userId);

//    @SuppressWarnings("JpaQlInspection")
    @Query("SELECT c from CreditApplication c WHERE c.dateTime BETWEEN :startDate AND :endDate ORDER BY c.dateTime DESC")
    List<CreditApplication> getBetween(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

//    @Query("SELECT m FROM CreditApplication m JOIN FETCH m.user WHERE m.id = ?1 and m.user.id = ?2")
//    CreditApplication getWithUser(int id, int userId);
}
