package org.testbank.repository.datajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.testbank.model.CreditApplication;
import org.testbank.repository.CreditApplicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;


@Repository
public class DataJpaCreditApplicationRepositoryImpl implements CreditApplicationRepository {
    private static final Logger LOG = LoggerFactory.getLogger(DataJpaCreditApplicationRepositoryImpl.class);

    @Autowired
    private CrudApplicationRepository crudApplicationRepository;

    @Autowired
    private CrudUserRepository crudUserRepository;

    @Override
    public CreditApplication save(CreditApplication app) {
        return crudApplicationRepository.save(app);
    }

    @Override
    public List<CreditApplication> getBetween(LocalDateTime startDate, LocalDateTime endDate) {
        return crudApplicationRepository.getBetween(startDate, endDate);
    }

//    @Override
//    @Transactional
//    public boolean delete(int id) {
//        LOG.info("public boolean delete(int id)");
//        return false;


//        return em.createNamedQuery(CreditApplication.DELETE)
//                .setParameter("id", id)
//                .setParameter("userId")
//                .executeUpdate() != 0;
//    }

    @Override
    public CreditApplication get(long id) {
        return crudApplicationRepository.findOne(id);
    }
}