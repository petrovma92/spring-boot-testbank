package org.testbank.repository.datajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.testbank.model.Settings;
import org.testbank.model.User;
import org.testbank.repository.SettingsRepository;
import org.testbank.repository.UserRepository;


@Repository
public class DataJpaSettingsRepositoryImpl implements SettingsRepository {
    private static final Logger LOG = LoggerFactory.getLogger(DataJpaSettingsRepositoryImpl.class);

    @Autowired
    private CrudSettingsRepository crudRepository;

    @Override
    public Settings save(Settings settings) {
        return crudRepository.save(settings);
    }

    @Override
    public Settings get(long id) {
        return crudRepository.findOne(id);
    }

    @Override
    public Settings getByKey(String key) {
        return crudRepository.getByKey(key);
    }
}
