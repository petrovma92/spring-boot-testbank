package org.testbank.repository.datajpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import org.testbank.model.Settings;
import org.testbank.model.User;

@Transactional(readOnly = true)
public interface CrudSettingsRepository extends JpaRepository<Settings, Long> {

    @Override
    @Transactional
    Settings save(Settings user);

    @Override
    Settings findOne(Long id);

    @Query("SELECT s FROM Settings s WHERE s.key = ?1")
    Settings getByKey(String key);
}
