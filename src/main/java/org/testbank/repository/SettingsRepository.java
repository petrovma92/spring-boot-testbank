package org.testbank.repository;


import org.testbank.model.Settings;

public interface SettingsRepository {
    Settings save(Settings user);
    Settings get(long id);
    Settings getByKey(String key);
}

