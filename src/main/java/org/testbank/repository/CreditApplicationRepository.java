package org.testbank.repository;



import org.testbank.model.CreditApplication;

import java.time.LocalDateTime;
import java.util.Collection;

public interface CreditApplicationRepository {

    // null if updated creditApplication do not belong to userId
    CreditApplication save(CreditApplication creditApplication);

    // false if request do not belong to userId
//    boolean delete(int id);

    // ORDERED dateTime
    Collection<CreditApplication> getBetween(LocalDateTime startDate, LocalDateTime endDate);

    // null if request do not belong to userId
    CreditApplication get(long id);
}
