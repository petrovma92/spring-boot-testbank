package org.testbank.repository;


import org.testbank.model.User;

public interface UserRepository {
    User save(User user);

    // false if not found
//    boolean delete(int id);

    // null if not found
    User get(long id);

//    User getByPassport(long passport);

    // null if not found
//    User getByEmail(String email);

//    List<User> getAll();

//    default User getWithMeals(int id){
//        throw new UnsupportedOperationException();
//    }
}

