package org.testbank.mvc;

public class SendDataModel {
    private String firstName;
    private String secondName;
    private String passportSeries;
    private String passportNumber;
    private String phone;
    private String birthDate;
    private String work;
    private String salary;
    private String badHabits;
    private String car;
    private String richRelative;
    private String aircraft;
    private String contactPerson;
    private String age;
    private String prevApp;
    private String status;
    private String errText;
    private String HtmlClassName;

    public String getFirstName() {
        return firstName;
    }

    public SendDataModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSecondName() {
        return secondName;
    }

    public SendDataModel setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public SendDataModel setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
        return this;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public SendDataModel setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public SendDataModel setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public SendDataModel setBirthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public String getWork() {
        return work;
    }

    public SendDataModel setWork(String work)  {
        this.work = work;
        return this;
    }

    public String getSalary() {
        return salary;
    }

    public SendDataModel setSalary(String salary) {
        this.salary = salary;
        return this;
    }

    public String getBadHabits() {
        return badHabits;
    }

    public SendDataModel setBadHabits(String badHabits) {
        this.badHabits = badHabits;
        return this;
    }

    public String getCar() {
        return car;
    }

    public SendDataModel setCar(String car) {
        this.car = car;
        return this;
    }

    public String getRichRelative() {
        return richRelative;
    }

    public SendDataModel setRichRelative(String richRelative) {
        this.richRelative = richRelative;
        return this;
    }

    public String getAircraft() {
        return aircraft;
    }

    public SendDataModel setAircraft(String aircraft) {
        this.aircraft = aircraft;
        return this;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public SendDataModel setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
        return this;
    }

    public String getAge() {
        return age;
    }

    public SendDataModel setAge(String age) {
        this.age = age;
        return this;
    }

    public String getErrText() {
        return errText;
    }

    public SendDataModel setErrText(String errText) {
        this.errText = errText;
        return this;
    }

    public String getPrevApp() {
        return prevApp;
    }

    public SendDataModel setPrevApp(String prevApp) {
        this.prevApp = prevApp;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public SendDataModel setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getHtmlClassName() {
        return HtmlClassName;
    }

    public SendDataModel setHtmlClassName(String htmlClassName) {
        this.HtmlClassName = htmlClassName;
        return this;
    }
}
