package org.testbank.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Dasha on 15.10.2017.
 */

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "id_passport", nullable = false)
    protected Long idPassport;

/*
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_id")
    private CreditApplication lastApplication;
    */

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "second_name", nullable = false)
    private String secondName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, mappedBy = "user")
    @OrderBy("dateTime DESC")
    protected List<CreditApplication> apps;

/*
    public CreditApplication getLastApplication() {
        return lastApplication;
    }
*/
    public String getFirstName() {
        return firstName;
    }
    public String getSecondName() {
        return secondName;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Long getIdPassport() {
        return idPassport;
    }

    public User setIdPassport(Long idPassport) {
        this.idPassport = idPassport;
        return this;
    }

    /*
        public User setLastApplication(CreditApplication lastApplication) {
            this.lastApplication = lastApplication;
            return this;
        }
    */
    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }
    public User setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }
    public User setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public List<CreditApplication> getApps() {
        return apps;
    }

    @Override
    public String toString() {
        return "User (" +
                "idPassport=" + idPassport +
                ", firstName=" + firstName +
                ", secondName=" + secondName +
//                ", lastApplicationId=" + lastApplication/*.getId()*/ +
                ", birthDate=" + birthDate/*.getId()*/ +
                ')';
    }
}
