package org.testbank.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "settings")
public class Settings {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_SEQ")
    @SequenceGenerator(sequenceName = "application_seq", allocationSize = 100000, name = "APP_SEQ")
    private Long id;

    @NotNull
    @Column(name = "KEY", nullable = false)
    private String key;

    @Column(name = "VALUE")
    private String value;

    public Long getId() {
        return id;
    }

    public Settings setId(Long id) {
        this.id = id;
        return this;
    }

    public String getKey() {
        return key;
    }

    public Settings setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Settings setValue(String value) {
        this.value = value;
        return this;
    }
}
