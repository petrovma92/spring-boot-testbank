-- drop table applications;
-- drop table users;
-- drop sequence customer_seq;
-- create sequence customer_seq start with 1 increment by 1;
-- create table applications (id NUMERIC not null, aircraft boolean not null, bad_habits boolean not null, car VARCHAR2(255), contact_person VARCHAR2(255), date_time TIMESTAMP(255) not null, phone VARCHAR2(255), previous_app_id NUMERIC, rich_relative boolean not null, salary integer check (salary>=0 AND salary<=9223372036854775807), scoring_value integer check (scoring_value>=0 AND scoring_value<=2000), status VARCHAR2(255), work VARCHAR2(255) not null, user_id NUMERIC not null, primary key (id));
-- create table users (id bigint not null, birth_date timestamp, first_name VARCHAR2(255) not null, second_name VARCHAR2(255) not null, app_id bigint not null, primary key (id));
-- alter table applications add constraint FKfsfqljedcla632u568jl5qf3w foreign key (user_id) references users;
-- alter table users add constraint FK5j4ubaupdyym5ro2hihna7t60 foreign key (app_id) references applications;

create table APPLICATIONS
(
    ID NUMBER(19) not null primary key,
    STATUS VARCHAR2(255 char),
    SCORING_VALUE NUMBER(10) check (scoring_value<=2000 AND scoring_value>=0),
    WORK VARCHAR2(255 char) not null,
    SALARY NUMBER(10) check (salary<=9223372036854775807 AND salary>=0),
    DATE_TIME TIMESTAMP(6) not null,
    CAR VARCHAR2(255 char),
    BAD_HABITS NUMBER(1) not null,
    RICH_RELATIVE NUMBER(1) not null,
    AIRCRAFT NUMBER(1) not null,
    PHONE VARCHAR2(255 char),
    CONTACT_PERSON VARCHAR2(255 char),
    PREVIOUS_APP_ID NUMBER(19),
    USER_PASSPORT NUMBER(19) not null
);

-- todo CHANGE DIALECT FOR CREATE SEQUENCE
CREATE SEQUENCE APPLICATION_SEQ;


create table USERS
(
    ID_PASSPORT NUMBER(19) not null primary key,
    FIRST_NAME VARCHAR2(255 char) not null,
    SECOND_NAME VARCHAR2(255 char) not null,
    BIRTH_DATE DATE
);

ALTER TABLE APPLICATIONS
    ADD CONSTRAINT USERS_APPLICATIONS_ID_fk FOREIGN KEY (USER_PASSPORT) REFERENCES USERS (ID_PASSPORT);

-- CREATE UNIQUE INDEX PASSPORT_INDEX ON USERS (ID_PASSPORT);

ALTER TABLE APPLICATIONS
    ADD CONSTRAINT APP_APP_ID_fk FOREIGN KEY (PREVIOUS_APP_ID) REFERENCES APPLICATIONS (ID);

/*
CREATE TABLE settings
(
    id INT PRIMARY KEY,
    key VARCHAR2(50),
    value VARCHAR2(50)
);
CREATE UNIQUE INDEX settings_key_uindex ON settings (key);
*/
