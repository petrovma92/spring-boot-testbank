package org.testbank;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Util {

    public static final Integer DATA_COUNT = 20_000;
    public static final String FILE_READ = "src\\test\\resources\\outPutWrite.txt";
    public static final String FILE_OUTPUT = "src\\test\\resources\\outPutWrite.txt";

    public static List<String> readFile(String fileName) {
        ArrayList<String> result = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(result::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void writeFile(String fileName, List<String> result) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName))) {
            result.forEach(e -> {
                try {
                    writer.write(e);
                    writer.newLine();
                } catch(IOException ex) {
                    ex.printStackTrace();
                    throw new RuntimeException(ex);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> addParameter(String paramName, List<String> paramValueList, List<String> paramList) {
        String[] params = paramList.get(0).split(",");
        int paramNumber = -1;
        for(int i = 0; i < params.length; i++)
            if(paramName.equals(params[i])) {
                paramNumber = i;
                break;
            }
        if(paramNumber < 0)
            throw new RuntimeException(String.format("Parameter '%s' not Exist!", paramName));
        String[] arr;
        StringBuilder s;
        String value;
        int n;

        for (int i = 1; i < paramList.size(); i++) {
            arr = paramList.get(i).split(",", -1);
            n = 0;
            value = paramValueList.size() > i-1 ? paramValueList.get(i-1) : "";
            s = new StringBuilder();
            while (params.length > n) {
                if (n == paramNumber) {
                    if (paramNumber < params.length-1)
                        s.append(value).append(',');
                    else
                        s.append(value);
                }
                else if(n < arr.length) {
                    if(params.length-1 > n) s.append(arr[n]).append(',');
                    else s.append(arr[n]);
                } else {
                    if(params.length-1 > n) s.append(',');
//                    else s.append(' ');
                }
                n++;
            }
            paramList.remove(i);
            paramList.add(i, s.toString());
        }

        return paramList;
    }
}
