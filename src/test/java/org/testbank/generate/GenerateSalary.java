package org.testbank.generate;

import org.testbank.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.testbank.Util.DATA_COUNT;
import static org.testbank.Util.FILE_READ;

public class GenerateSalary {
    private static String fileRead = "src\\test\\resources\\outPut.txt";
    private static String fileOutput = "src\\test\\resources\\outPutWrite.txt";

    public static void main(String... args) {
        int dataCount = DATA_COUNT != null && DATA_COUNT > 0 ? DATA_COUNT : 10;
        fileRead = FILE_READ != null ? FILE_READ : fileRead;
        fileOutput = FILE_READ != null ? FILE_READ : fileOutput;


        List<String> data = Util.readFile(fileRead);

        Util.writeFile(fileOutput,
                Util.addParameter("salary", testData(dataCount), data));
    }

    private static List<String> testData(int dataCount) {
        ArrayList<String> generated = new ArrayList<>();

        int percent;
        for(int i = 0; i < dataCount; i++) {
            percent = ThreadLocalRandom.current().nextInt(0, 100 + 1);

            if (percent < 2) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(200, 1000 + 1) * 1000));
            }
            else if (percent < 6) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(100, 200 + 1) * 1000));
            }
            else if (percent < 15) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(80, 100 + 1) * 1000));
            }
            else if (percent < 35) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(50, 80 + 1) * 1000));
            }
            else if (percent < 57) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(30, 50 + 1) * 1000));
            }
            else if (percent < 80) {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(15, 30 + 1) * 1000));
            }
            else {
                generated.add(String.valueOf(ThreadLocalRandom.current().nextInt(1, 15 + 1) * 1000));
            }
        }
        return generated;
    }
}