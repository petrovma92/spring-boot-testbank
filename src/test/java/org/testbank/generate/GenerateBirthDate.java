package org.testbank.generate;

import org.testbank.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.testbank.Util.DATA_COUNT;
import static org.testbank.Util.FILE_READ;

public class GenerateBirthDate {
    private static String fileRead = "src\\test\\resources\\outPut.txt";
    private static String fileOutput = "src\\test\\resources\\outPutWrite.txt";

    public static void main(String... args) {
        int dataCount = DATA_COUNT != null && DATA_COUNT > 0 ? DATA_COUNT : 10;
        fileRead = FILE_READ != null ? FILE_READ : fileRead;
        fileOutput = FILE_READ != null ? FILE_READ : fileOutput;


        List<String> data = Util.readFile(fileRead);

        Util.writeFile(fileOutput,
                Util.addParameter("birthDate", testData(dataCount), data));
    }

    private static List<String> testData(int dataCount) {
        ArrayList<String> generated = new ArrayList<>();


        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date randomDate, d1, d2;
        String birthDate;

        try {
            d1 = formatter.parse("1920-01-01");
            d2 = formatter.parse("2006-01-01");
            int percent;
            for(int i = 0; i < dataCount; i++) {
                percent = ThreadLocalRandom.current().nextInt(0, 100 + 1);

                if (percent < 10) {
                    birthDate = "";
                }
                else {
                    randomDate = new Date(ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime()));
                    birthDate = formatter.format(randomDate);
                }
                generated.add(birthDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return generated;
    }
}