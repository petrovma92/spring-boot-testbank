package org.testbank.generate.names;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

/**
 * Created by Tester on 04.05.2017.
 */
public class GenerateTestDataNameSurname {
    private static String FILE_NAME_1 = "src\\test\\resources\\test1.txt";
    private static String FILE_NAME_2 = "src\\test\\resources\\test2.txt";
    //	private static String FILE_NAME_1 = "test1test.txt";
//	private static String FILE_NAME_2 = "test2test.txt";
    private static List<String> manName;
    private static List<String> manSurname;
    private static List<String> womanName;
    private static List<String> womanSurname;
    private static List<String> dataWoman;
    private static List<String> dataMan;

    public static void main(String... args) throws Exception{


        manName = new ArrayList<>();
        manSurname = new ArrayList<>();
        womanName = new ArrayList<>();
        womanSurname = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(FILE_NAME_1))) {
            stream.forEach(e -> test(e, manName, manSurname));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (Stream<String> stream = Files.lines(Paths.get(FILE_NAME_2))) {
            stream.forEach(e -> test(e, womanName, womanSurname));
        } catch (IOException e) {
            e.printStackTrace();
        }

        dataMan = generateTestData(manName, manSurname, 10_000);

        dataWoman = generateTestData(womanName, womanSurname, 10_000);
        for(String t : dataMan) System.out.println(t);
        System.out.println("====================================================================================================================================");
        for(String t : dataWoman) System.out.println(t);

    }

    private static List<String> generateTestData(List<String> name, List<String> surname, int dataCount) {
        String s;
        ArrayList<String> data = new ArrayList<>();

        for(int i = 0; i < dataCount; i++) {
            if(i < name.size() && i < surname.size())
                data.add(surname.get(i) + "," + name.get(i));
            else if(i < name.size()) {
                do {
                    s = getRandomElement(surname) + "," + name.get(i);
                } while (data.contains(s));
                data.add(s);
            }
            else if(i < surname.size()) {
                do {
                    s = surname.get(i) + "," + getRandomElement(name);
                } while (data.contains(s));
                data.add(s);
            }
            else {
                do {
                    s = getRandomElement(surname) + "," + getRandomElement(name);
                } while (data.contains(s));
                data.add(s);
            }
        }
        return data;
    }

    private static void test(String s, List<String> name, List<String> surname) {
        String[] arr  = s.split("\\|");
        for(int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replaceAll("\\s*([A-я]+)\\s*", "$1");
        }
        if(arr.length == 3) {
            if(!arr[1].isEmpty() && !arr[1].matches("\\s+")) {
                surname.add(arr[1]);
            }
            if(!arr[2].isEmpty() && !arr[2].matches("\\s+")) {
                name.add(arr[2]);
            }
        }
        else if(arr.length == 1) surname.add(arr[0]);
        else throw new RuntimeException();
    }

    private static String getRandomElement(List<String> arr) {
        return arr.get(ThreadLocalRandom.current().nextInt(0, arr.size()));
    }
}
