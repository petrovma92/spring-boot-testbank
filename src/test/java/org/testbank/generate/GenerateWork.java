package org.testbank.generate;

import org.testbank.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.testbank.Util.DATA_COUNT;
import static org.testbank.Util.FILE_READ;

public class GenerateWork {
    private static String fileRead = "src\\test\\resources\\outPut.txt";
    private static String fileOutput = "src\\test\\resources\\outPutWrite.txt";

    public static void main(String... args) {
        int dataCount = DATA_COUNT != null && DATA_COUNT > 0 ? DATA_COUNT : 10;
        fileRead = FILE_READ != null ? FILE_READ : fileRead;
        fileOutput = FILE_READ != null ? FILE_READ : fileOutput;


        List<String> data = Util.readFile(fileRead);

        Util.writeFile(fileOutput,
                Util.addParameter("work", testData(dataCount), data));
    }

    private static List<String> testData(int dataCount) {
        ArrayList<String> generated = new ArrayList<>();

        int percent;
        for(int i = 0; i < dataCount; i++) {
            percent = ThreadLocalRandom.current().nextInt(0, 100 + 1);

            if (percent < 33) {
                generated.add("YES");
            }
            else if (percent < 66) {
                generated.add("NO");
            }
            else {
                generated.add("HZ");
            }
        }
        return generated;
    }
}